import torch
from torch import Tensor
from torch import nn

# user data structures
from dataclasses import dataclass

@dataclass ## wait for 3.10 so I can use dataclass(kw_only=True)
class MultiHeadAttentionParams:
    num_heads: int
    dim_in: int
    dim_k: int
    dim_v: int

@dataclass
class HeadAttentionParams:
    dim_in: int
    dim_k: int
    dim_v: int

@dataclass
class ResidualParams:
    sublayer: nn.Module
    dimension: int
    dropout: float = 0.1

@dataclass
class TransformerEncoderParams:
    dim_model: int = 512
    num_heads: int = 6
    dim_hidden: int = 2048
    dropout: float = 0.1

@dataclass
class FeedForwardParams:
    dim_in: int
    dim_hidden: int

# build test inputs and set parameters
## random seed
seed = 278654169578
key = torch.Generator().manual_seed(seed)

## set the generation objects
device = torch.device('cpu')
kwargs = {'device': device, 'generator':key}

## parameters
batch_size = 3
seq_length = 8
num_features = 5

## create matrices
Q = torch.randn([batch_size, seq_length, num_features], **kwargs) ### query
K = torch.randn([batch_size, seq_length, num_features], **kwargs) ### key
V = torch.randn([batch_size, seq_length, num_features], **kwargs) ### value

## utility
def arg_extract(args , var: list[str]): ### doesn't check args type
    _args = vars(args)
    return (_args[elem] for elem in var)

# define torch modules for transformer
class HeadAttention(nn.Module):
    def __init__(self, args: HeadAttentionParams):
        """Attention head consists of 3 linear layers"""
        super().__init__()
        self.q = nn.Linear(args.dim_in, args.dim_k)
        self.k = nn.Linear(args.dim_in, args.dim_k)
        self.v = nn.Linear(args.dim_in, args.dim_v)
        self.args = p

    def forward(self, query: Tensor, key: Tensor, value: Tensor) -> Tensor:
        """transform inputs with linear layers then apply sdp attention"""
        Q = self.q(query)
        K = self.k(key)
        V = self.v(value)
        return self.sdp_attention(Q, K, V)

    def sdp_attention(self, Q: Tensor, K: Tensor, V: Tensor) -> Tensor:
        """scaled dot product attention: applies the function
        softmax(QK^t/n**0.5) V"""
        ### scaled dot-product attention Q, K, V
        QKt = Q @ K.transpose(1, 2) ### other = Q.bmm(K.transpose(1, 2))
        scale = Q.size(-1) ** 0.5 ### num features scaling
        ### omit mask operation
        softmax = (QKt / scale).softmax(dim=-1)
        result = softmax @ V
        return result

class MultiHeadAttention(nn.Module):
    def __init__(self, args: MultiHeadAttentionParams):
        """MHA has multiple Attention heads, and a final Linear Layer"""
        super().__init__()
        sub_args = HeadAttentionParams(args.dim_in, args.dim_k, args.dim_v)
        self.heads = nn.ModuleList(
            [HeadAttention(sub_args) for _ in range(args.num_heads)])
        self.linear = nn.Linear(args.num_heads * args.dim_v, args.dim_in)
        self.args = p

    def forward(self, query: Tensor, key: Tensor, value: Tensor) -> Tensor:
        """concat output of heads and feed into linear layer"""
        x = [head(query, key, value) for head in self.heads]
        x = torch.cat(x, dim=-1) ### concat along the features
        return self.linear(x)

class Residual(nn.Module):
    """sub modules which add residule to output of sublayer"""
    def __init__(self, args: ResidualParams):
        super().__init__()
        self.sublayer = args.sublayer
        self.norm = nn.LayerNorm(args.dimension)
        self.dropout = nn.Dropout(args.dropout)
        self.args = p

    def forward(self, *tensors: Tensor) -> Tensor:
        """V is assumed to be the last tensor"""
        x = tensors[-1] + self.dropout(self.sublayer(*tensors))
        x = self.norm(x)
        return x

class FeedForward(nn.Module):
    """feed forward component of the transformer Linear -> Relu -> Linear"""
    def __init__(self, args: FeedForwardParams):
        self.net = nn.Sequential(
            nn.Linear(args.dim_in, args.dim_hidden), nn.ReLU(),
            nn.Linear(args.dim_hidden, args.dim_in))
        self.args(p)

    def forward(self, x):
        return self.net(x)

# utility function defs
def position_encoding(seq_len, dim_model, device=torch.device('cpu')):
    kwargs = {'dtype':torch.float, 'device':device}
    pos = torch.arange(seq_len, **kwargs).reshape(1, -1, 1)
    dim = torch.arange(dim_model, **kwargs).reshape(1, 1, -1)
    phase = pos / 1e4 ** (dim // dim_model)
    return torch.where(dim.long() % 2 == 0, torch.sin(phase), torch.cos(phase))

# test code
mha_args = MultiHeadAttentionParams(2, 5, 3, num_features)
layer = MultiHeadAttention(mha_args)
residual_args = ResidualParams(layer, num_features, 0.1)
model = Residual(residual_args)
import pdb;pdb.set_trace()
